//Actions are similar to mutations, the differences being that:

//Instead of mutating the state, actions commit mutations.
//Actions can contain arbitrary asynchronous operations.

import * as types from './mutation-types'

export const initJokes = ({commit}) => {
  fetch('https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_ten', {
    method: 'GET'
  })
  .then(response => response.json())
  .then(json => commit(types.INIT_JOKES, json))
}


export const addJoke = ({commit}) => {
  fetch('https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_joke', {
    method: 'GET'
  })
  .then(response => response.json())
  .then(json => commit(types.ADD_JOKES, json))
}

export const removeJoke = ({commit}, index) => {
  commit(types.REMOVE_JOKE, index)
}
