import * as types from './mutation-types'

//The only way to actually change state in a Vuex store is by committing a mutation

//Vuex mutations are very similar to events: each mutation has a string type and a handler

//The handler function is where we perform actual state modifications, and it will receive the state as the first argument.

//You can pass an additional argument to store.commit, which is called the payload for the mutation:

export const mutations = {
  [types.INIT_JOKES] (state, payload) {
  state.jokes.push(...payload)
},

[types.ADD_JOKES] (state, payload) {
  state.jokes.push(payload)
},

[types.REMOVE_JOKE] (state, index) {
  state.jokes.splice(index, 1)
}
}
