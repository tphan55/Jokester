export const INIT_JOKES = 'INIT_JOKES'
export const ADD_JOKES = 'ADD_JOKES'
export const REMOVE_JOKE = 'REMOVE_JOKE'
//It is a commonly seen pattern to use constants for mutation types in various Flux implementations.
//This allows the code to take advantage of tooling like linters,
// and putting all constant in a single file allows your collaborators to get an at-a-glance view of what mutations
//are possible in the entire application
