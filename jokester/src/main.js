import Vue from 'vue'
import App from './components/App.vue'

import store from './store'

// You cannot directly call a mutation handler.
//Think of it more like event registration: "When a mutation with type increment is triggered, call this handler."
//To invoke a mutation handler, you need to call store.commit with its type.

//In most cases, the payload should be an object so that it can contain multiple fields, and the recorded mutation will also be more descriptive.

// store.commit('INIT_JOKES', [{test: 'test_joke' }])

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
